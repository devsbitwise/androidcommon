/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.android.kotlin) // This should be put before any Kotlin dependent plugin
}

android {
    namespace = "com.devsbitwise.android.common"
    compileSdk = 35

    defaultConfig {

        aarMetadata {
            minCompileSdk = 35 // Always the same with the above compileSdk, consumer's compiledSdk is also required to follow this
        }

        minSdk = 21

        version = "2.2.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")

    }

    buildTypes {
        debug {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        release {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }
    buildFeatures {
        viewBinding = true
        buildConfig = true
    }
    testOptions {
        targetSdk = 23
    }
}

dependencies {

    testImplementation(libs.junit)
    testImplementation(libs.google.truth)
    testImplementation(libs.koin.test) // To perform dependency injection using Koin for unit test

    androidTestImplementation(libs.androidx.runner) // Adding @SmallTest, @MediumTest, @LargeTest and enable this module to run instrumented test
    androidTestImplementation(libs.androidx.junit.ext)
    androidTestImplementation(libs.androidx.junit.ext.ktx)
    androidTestImplementation(libs.androidx.truth.ext)
    androidTestImplementation(libs.google.truth)
    androidTestImplementation(libs.koin.android.test) // To perform dependency injection using Koin for instrumented test

    // Foundation
    api(libs.appcompat)
    api(libs.annotation)

    // Android KTX
    api(libs.core.ktx)
    api(libs.activity.ktx)
    api(libs.fragment.ktx)
    api(libs.collection.ktx)

    // UI
    api(libs.webkit) // Modern WebView APIs on Android 5 (L) and above
    api(libs.viewpager2)
    api(libs.recyclerview)
    api(libs.swipe.refresh.layout)

    // ViewModel
    api(libs.lifecycle.viewmodel)
    // Lifecycles only (without ViewModel or LiveData)
    api(libs.lifecycle.runtime.ktx)
    // LiveData
    api(libs.lifecycle.livedata.ktx)
    // Saved state module for ViewModel
    api(libs.lifecycle.viewmodel.savedstate)

    // Contains common APIs such as Dispatchers, launch, withContext, etc.
    api(libs.kotlinx.coroutines.core)
    // Contains platform specific APIs or implementation such as Android version of Dispatchers.Main
    api(libs.kotlinx.coroutines.android)

    // Koin
    api(libs.koin.android)

    // Glide
    api(libs.glide)

    // Play In-App Update
    api(libs.app.update.ktx)

}