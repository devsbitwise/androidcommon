# Android Common Module
This module contains classes, extensions, and frameworks that are commonly use when developing an Android project.
The objective is to accelerate the development by providing instances of recommended Android framework or library using Koin, extension functions, base classes, resources, and tests.


## Tools and Frameworks
- **Glide** - image loading and caching library for Android
- **Play In-App Update** - library for app updates using Google Play Services
- **View Binding** - generates view reference, replacing the old `findViewById`
- **Lifecycle-aware Components** - library for managing lifecycles in Android
- **Koin (Android)** - Kotlin dependency injection framework with platform specific implementation
- **Kotlin Coroutines (Android)** - Kotlin library for asynchronous programming with platform specific implementation
- **Kotlin Flow**, **Kotlin Channel**, **StateFlow**, **SharedFlow**, **LiveData** - observable data holder


## Prerequisite
This module assumed that you have prior experience on using the following:

- **Version Catalog** - Required
- **Koin** - Recommended


## Installation
Add the following to your project level `build.gradle` dependencies block.
```kotlin
// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.android.application).apply(false)
    alias(libs.plugins.android.kotlin).apply(false)
}
```

Add the following to your app level `build.gradle` dependencies block.
```kotlin
dependencies {
    implementation(project(":AndroidCommon"))
}
```

Add the following to your `settings.gradle`.
```kotlin
include(":AndroidCommon")
```

On your default TOML file `libs.versions.toml` add the following dependencies.
```toml
[versions]
core-ktx = "1.13.1"
appcompat = "1.7.0"
annotation = "1.8.2"
activity-ktx = "1.9.2"
fragment-ktx = "1.8.3"
collection-ktx = "1.4.4"

lifecycle-runtime = "2.8.6"

webkit = "1.12.0"
viewpager2 = "1.1.0"
recyclerview = "1.3.2"
swiperefreshlayout = "1.1.0"

truth = "1.4.4"
runner = "1.6.2"
junit = "4.13.2"
androidx-junit = "1.2.1"
androidx-truth = "1.6.0"

kotlinx-coroutines = "1.9.0"

koin = "4.0.0"

glide = "4.16.0"

app-update-ktx = "2.1.0"

kotlin = "2.0.20"

[libraries]
# AndroidX Core
core-ktx = { module = "androidx.core:core-ktx", version.ref = "core-ktx" }
appcompat = { module = "androidx.appcompat:appcompat", version.ref = "appcompat" }
annotation = { module = "androidx.annotation:annotation", version.ref = "annotation" }
activity-ktx = { module = "androidx.activity:activity-ktx", version.ref = "activity-ktx" }
fragment-ktx = { module = "androidx.fragment:fragment-ktx", version.ref = "fragment-ktx" }
collection-ktx = { module = "androidx.collection:collection-ktx", version.ref = "collection-ktx" }

# AndroidX Lifecycle
lifecycle-runtime-ktx = { module = "androidx.lifecycle:lifecycle-runtime-ktx", version.ref = "lifecycle-runtime" }
lifecycle-livedata-ktx = { module = "androidx.lifecycle:lifecycle-livedata-ktx", version.ref = "lifecycle-runtime" }
lifecycle-viewmodel = { module = "androidx.lifecycle:lifecycle-viewmodel", version.ref = "lifecycle-runtime" }
lifecycle-viewmodel-savedstate = { module = "androidx.lifecycle:lifecycle-viewmodel-savedstate", version.ref = "lifecycle-runtime" }

# AndroidX UI
webkit = { module = "androidx.webkit:webkit", version.ref = "webkit" }
viewpager2 = { module = "androidx.viewpager2:viewpager2", version.ref = "viewpager2" }
recyclerview = { module = "androidx.recyclerview:recyclerview", version.ref = "recyclerview" }
swipe-refresh-layout = { module = "androidx.swiperefreshlayout:swiperefreshlayout", version.ref = "swiperefreshlayout" }

# Test
junit = { module = "junit:junit", version.ref = "junit" }
androidx-runner = { module = "androidx.test:runner", version.ref = "runner" }
# Test Assertions
androidx-truth-ext = { module = "androidx.test.ext:truth", version.ref = "androidx-truth" }
androidx-junit-ext = { module = "androidx.test.ext:junit", version.ref = "androidx-junit" }
androidx-junit-ext-ktx = { module = "androidx.test.ext:junit-ktx", version.ref = "androidx-junit" }
google-truth = { module = "com.google.truth:truth", version.ref = "truth" }

# Coroutine
kotlinx-coroutines-core = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-core", version.ref = "kotlinx-coroutines" }
kotlinx-coroutines-android = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-android", version.ref = "kotlinx-coroutines" }

# Koin
koin-android = { module = "io.insert-koin:koin-android", version.ref = "koin" }
koin-test = { module = "io.insert-koin:koin-test", version.ref = "koin" }
koin-android-test = { module = "io.insert-koin:koin-android-test", version.ref = "koin" }

# Glide
glide = { module = "com.github.bumptech.glide:glide", version.ref = "glide" }

# Play In-App Update
app-update-ktx = { module = "com.google.android.play:app-update-ktx", version.ref = "app-update-ktx" }

[plugins]
android-library = { id = "com.android.library" }
android-kotlin = { id = "org.jetbrains.kotlin.android", version.ref = "kotlin" }
```


## Usage
Add the Koin module `androidCommonModule` in your class that starts Koin.
```kotlin
class App : Application() {
    
    override fun onCreate() {
        super.onCreate()

        startKoin {

            androidLogger()
            androidContext(this@App)
            modules(commonModule, androidCommonModule)

        }

    }

}
```


#### Glide
The Glide instance provided by this library uses Application context so no need to worry about handling a destroyed context of Activity or Fragment, the example below is using Koin to perform field injection.
```kotlin
private val glide by inject<RequestManager>()

// Usage
glide.load(source).into(imageView)
```


#### SharedPreferences
This library has custom implementation of `SharedPreferences`, the example below is using Koin to perform field injection.
```kotlin
private val userPreferences by inject<UserPreferences>()

// Get value
userPreferences.prefBoolean("pref_key")

// Set value
userPreferences.prefBoolean("pref_key", true)
```


#### In-App Update
This module provides extension function for In-App Update to simplify its implementation, the example below is using Koin to perform field injection.
```kotlin
class MainActivity : AppCompatActivity(), InstallStateUpdatedListener {

    private val binding get() = _binding!!

    private var _binding: ActivityMainBinding? = null

    private var isAppUpdateOngoing = false

    private lateinit var inAppUpdateDialog: AlertDialog

    private val installStateUpdatedListener = InstallStateUpdatedListener { state ->

        when (state.installStatus()) {

            InstallStatus.DOWNLOADED -> {

                if (isDestroyed.not() && isFinishing.not()) {
                    inAppUpdateDialog.show()
                }

            }

            InstallStatus.INSTALLED -> {
                unregisterInAppUpdateListener()
            }

            InstallStatus.CANCELED, InstallStatus.FAILED, InstallStatus.UNKNOWN -> {
                Log.e("TAG", "In-App Update Failed Status Code: ${state.installErrorCode()}")
                unregisterInAppUpdateListener()
            }

            InstallStatus.DOWNLOADING,
            InstallStatus.INSTALLING,
            InstallStatus.PENDING -> {
                /* no-op */
            }

            else -> {
                Log.e("TAG", "In-App Update Unknown Status Code: ${state.installErrorCode()}")
                unregisterInAppUpdateListener()
            }

        }

    }
    
    private lateinit var inAppUpdateResultLauncher: ActivityResultLauncher<IntentSenderRequest>

    private val appUpdateManager by inject<AppUpdateManager>()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // In case where Activity was recreated, reinitialize
        inAppUpdateResultLauncher = activityForResultIntentSender { result, _ ->

            when (result.resultCode) {
                // In flexible flow, the user accepted the request to update.
                // In immediate flow, the user accepted and the update succeeded (which, in practice, your app never should never receive because it already updated).
                Activity.RESULT_OK -> isAppUpdateOngoing = true

                // In flexible flow, the user denied the request to update.
                // In immediate flow, the user denied or canceled the update.
                Activity.RESULT_CANCELED -> unregisterInAppUpdateListener()

                // In flexible flow, something failed during the request for user confirmation. For example, the user terminates the app before responding to the request.
                // In immediate flow, the flow failed either during the user confirmation, the download, or the installation.
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    Log.e("TAG", "In-App Update Unknown Failure")
                    showToast(getString(R.string.unknown_error))
                }
            }

        }

        inAppUpdateDialog = MaterialAlertDialogBuilder(this).apply {
            setTitle("An update has just been downloaded.")
            setPositiveButton("Update and Restart", { dialog, _ ->
                appUpdateManager.completeUpdate() // Install updates
                dialog.dismiss()
            })
            setCancelable(false)
        }

        // Recover the the UI state during configuration change or system-initiated process death
        isAppUpdateOngoing = savedInstanceState?.getBoolean("isAppUpdateOngoing", false) == true

    }

    override fun onSaveInstanceState(outState: Bundle) {
        // Store the UI state that can survive even a System-initiated system-initiated process death
        outState.putBoolean("isAppUpdateOngoing", isAppUpdateOngoing)
        super.onSaveInstanceState(outState)
    }

    // Lifecycles

    override fun onStart() {
        super.onStart()
        if (isAppUpdateOngoing.not()) {
            appUpdateManager.checkUpdates(inAppUpdateResultLauncher, installStateUpdatedListener)
        }
    }

    override fun onDestroy() {
        inAppUpdateDialog.dismiss()
        super.onDestroy()
    }

    private fun unregisterInAppUpdateListener() {
        appUpdateManager.unregisterListener(installStateUpdatedListener)
    }
    
}
```


#### Activity and Fragment
This module provides abstract base class of Activity and Fragment, this helps you to have a blueprint for consistent structure and pattern.

Extend your Activity or Fragment class with `BaseActivity` or `BaseFragment`.
```kotlin
class MainActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>() {

    // ActivityResultLauncher for requesting runtime multiple permission
    private val permissionResultLauncher = getPermission()

    // Just specify ViewModel as data type and set it to null if not needed
    override val viewModel: MainActivityViewModel by viewModels()

    // ViewBinding
    override fun bindingInflater(): (LayoutInflater) -> ActivityMainBinding =
            ActivityMainBinding::inflate

    // Equivalent of onCreate in Activity
    override fun initViews(binding: ActivityMainBinding) {
      
      // Extension to launch storage runtime permission 
      permissionResultLauncher.requestStoragePermission()

    }

    // This is called after initViews set your observer or subscriber/collector here
    override fun subscribeUI() {
    
    }
    
    // Callback result of runtime permission
    override fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {
        
        // If any from the permissions fail
        if (permissions.containsValue(false)) {
            // Permission denied
            return
        }
        
        // Permission granted
        
    }
    
}
```

The `BaseFragment` also includes an implementation of `OnBackPressedCallback` which imitates the old back press of Activity, override `onBackPressed` and set `interceptBackPress` to `true` in order to enable the Fragment to take control of the back press action.
```kotlin
class SampleFragment : BaseFragment<FragmentAboutBinding, SampleFragmentViewModel>() {

    // Intercept the back press for this Fragment
    override var interceptBackPress: Boolean = true

    // Sample approach when initializing ViewModel with Koin
    override val viewModel by viewModel<SampleFragmentViewModel>()

    // ViewBinding
    override fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> FragmentSampleBinding =
        FragmentSampleBinding::inflate

    // Equivalent of onCreateView in Fragment
    override fun initViews(binding: FragmentSampleBinding) {

    }

    // Equivalent of onViewCreated in Fragment, set your observable data holder or subscriber/collector here
    override fun subscribeUI() {
        with(viewModel) {
            viewLifecycleOwner.collectState(sampleState, ::onSampleStateChanged)
        }
    }

    private fun onSampleStateChanged(state: SampleState) {
        
        when (state) {

            is SampleState.FetchLoading -> {
                
            }

            is SampleState.FetchSuccess -> {

            }

            is SampleState.FetchFailed -> {

            }
            
        }
        
    }

    override fun onBackPressed() {
        if (someCondition) {
           // Intercepted back press
        } else {
            super.onBackPressed()
        }
    }

}
```

In addition, both `BaseActivity` and `BaseFragment` has `ActivityResultLauncher` to easily implement runtime permission. 

If your Activity or Fragment does not require a ViewModel, just specify `ViewModel` as data type and set it to `null`.

Check the `extensions` package to see all available runtime permission extension functions.

## Case Studies
This module is being used on the following projects:

- [**Cryptonian**](https://bitbucket.org/devsbitwise/cryptonian/src/master/)


## Notes
- In order to use the resources (R class) of this library when using AGP 8.x.x, set `android.nonTransitiveRClass` to `false` in `gradle.properties`.
- **Gradle 8.8.+** must be use when using this library which allows [Version Catalog plugin aliases without a version](https://docs.gradle.org/8.8/release-notes.html) such as `com.android.library`.


## Changelogs

###### v2.2
- Renamed source set layout from **java** to **kotlin**
- Added extension for getting **Serializable** from **Intent** and **Bundle**
- Added extension for getting **Parcelable** array from **Intent** and **Bundle**
- Improved full screen extensions `fullScreenNoStatusBar`, `fullScreenNoNavBar`, and `fullScreenNoSystemBars`
- Bump Android version support

###### v2.0
- **Koin** replacing **Hilt**
- Removed **Retrofit**, **Moshi**, and **Java 8+ APIs Desugar**
- Removed **CommonNotification** data class
- Removed **JsonParserRepository** and its concrete implementation
- Removed **View.createAndStoreScreenshot** in favor of **BitmapToFileRepository**
- Moved all Kotlin Multiplatform Mobile (KMM) compatible code to **[Kommon](https://bitbucket.org/devsbitwise/kommon/src/main/)** module

###### v1.5
- Migrated from **Dagger** to **Hilt**


# License
```markdown
MIT License

Copyright (c) 2022 Devs Bitwise

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```