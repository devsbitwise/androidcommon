package com.devsbitwise.android.common.utils

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.devsbitwise.android.common.domain.models.ValidationResult
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class ValidationUtilsEmailTest {

    @Test
    fun emptyEmail_isFalse() {
        val result = ValidationUtils.validateEmail("")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun invalidEmails_isFalse() {

        val invalidEmailList = listOf(
            "emmanuel.hibernate.org",
            "emma nuel@hibernate.org",
            "emma(nuel@hibernate.org",
            "emmanuel@",
            "emma\nnuel@hibernate.org",
            "emma@nuel@hibernate.org",
            "emma@nuel@.hibernate.org",
            "Just a string",
            "string",
            "me@",
            "@example.com",
            "me@example..com",
            "me\\@example.com",
            "Abc.example.com",
            "A@b@c@example.com",
            "a\"b(c)d,e:f;g<h>i[j\\k]l@example.com",
            "just\"not\"right@example.com",
            "this is\"not\\allowed@example.com",
            "this\\ still\\\"not\\\\allowed@example.com",
            "john.doe@example..com"
        )

        val listResult = mutableListOf<ValidationResult>()

        for (invalidEmail in invalidEmailList) {
            val result = ValidationUtils.validateEmail(invalidEmail)
            listResult.add(result)
        }

        assertThat(listResult.any { it.isSuccessful }).isFalse()

    }

    @Test
    fun validEmails_isTrue() {

        val validEmailList = listOf(
            "firstname.lastname@example.com",
            "email@subdomain.example.com",
            "firstname+lastname@example.com",
            "1234567890@example.com",
            "email@example-one.com",
            "_______@example.com",
            "email@example.name",
            "email@example.museum",
            "email@example.co.jp",
            "firstname-lastname@example.com"
        )

        val listResult = mutableListOf<ValidationResult>()

        for (validEmail in validEmailList) {
            val result = ValidationUtils.validateEmail(validEmail)
            listResult.add(result)
        }

        assertThat(listResult.all { it.isSuccessful }).isTrue()

    }

}