/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.data.data_source.local

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.core.content.FileProvider
import com.devsbitwise.android.common.domain.RequestStatus
import com.devsbitwise.android.common.domain.repositories.BitmapToFileRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

class BitmapToFileRepositoryImpl(private val context: Context) : BitmapToFileRepository {

    override fun storeBitmapToImageFile(bitmap: Bitmap, folderName: String) = flow {

        emit(RequestStatus.Loading<Uri?>())

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            val filePath = Environment.DIRECTORY_PICTURES + File.separator + folderName

            val contentValues = ContentValues().apply {
                put(
                    MediaStore.MediaColumns.DISPLAY_NAME,
                    UUID.randomUUID().toString() + ".png"
                )
                put(MediaStore.MediaColumns.MIME_TYPE, "image/png") // Cannot be */*
                put(MediaStore.Files.FileColumns.IS_PENDING, true)
                put(MediaStore.MediaColumns.RELATIVE_PATH, filePath)
            }

            val contentResolver = context.contentResolver

            val uriResolve =
                contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues
                )

            uriResolve?.let {

                try {

                    contentResolver.openOutputStream(uriResolve).use { stream ->

                        // This method may take several seconds to complete, so it should only be called from a worker thread.
                        // Read https://developer.android.com/reference/android/graphics/Bitmap#compress(android.graphics.Bitmap.CompressFormat,%20int,%20java.io.OutputStream)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream!!)

                        contentValues.put(MediaStore.Files.FileColumns.IS_PENDING, false)
                        contentResolver.update(uriResolve, contentValues, null, null)

                        emit(RequestStatus.Success(uriResolve))

                    }

                } catch (e: Exception) {
                    // Don't leave an orphan entry in the MediaStore
                    contentResolver.delete(uriResolve, null, null)
                    emit(RequestStatus.Failed<Uri?>(e))
                }

            } ?: run {
                emit(RequestStatus.Failed<Uri?>(IOException("Failed to create new MediaStore record.")))
            }

        } else {

            // Get primary storage status
            val state = Environment.getExternalStorageState()

            val filePath = File(Environment.getExternalStorageDirectory().absolutePath + File.separator + Environment.DIRECTORY_PICTURES + File.separator + folderName)

            if (Environment.MEDIA_MOUNTED == state) {

                filePath.mkdirs() // Returns true if new folder is created.

                // Create a new file
                val imageFile = File(filePath, UUID.randomUUID().toString() + ".png")

                try {

                    FileOutputStream(imageFile).use { stream ->

                        // This method may take several seconds to complete, so it should only be called from a worker thread.
                        // Read https://developer.android.com/reference/android/graphics/Bitmap#compress(android.graphics.Bitmap.CompressFormat,%20int,%20java.io.OutputStream)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)

                        // Notify the media scanner that new file has been added
                        // Source: https://stackoverflow.com/a/5814533/12204620
                        MediaScannerConnection.scanFile(
                            context.applicationContext,
                            arrayOf(imageFile.path),
                            null
                        ) { path, uri ->
                            Log.i("ExternalStorage", "Scanned: $path")
                            Log.i("ExternalStorage", "Uri: $uri")
                        }

                        val uriForFile = FileProvider.getUriForFile(
                            context.applicationContext,
                            "${context.applicationContext.packageName}.provider",
                            imageFile
                        )

                        emit(RequestStatus.Success(uriForFile))

                    }

                } catch (e: Exception) {
                    emit(RequestStatus.Failed<Uri?>(e))
                }

            } else {
                emit(RequestStatus.Failed<Uri?>(IOException("No available storage.")))
            }

        }

    }.flowOn(Dispatchers.IO) // Overwrite the CoroutineContext of producer: https://developer.android.com/kotlin/flow#context

}