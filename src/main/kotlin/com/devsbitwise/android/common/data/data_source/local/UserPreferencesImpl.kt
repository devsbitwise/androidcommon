/*
 * Copyright (c) 2023 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.data.data_source.local

import android.content.SharedPreferences
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_BOOLEAN
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_FLOAT
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_INTEGER
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_LONG
import com.devsbitwise.android.common.utils.DEFAULT_VALUE_STRING

class UserPreferencesImpl(private val sharedPreferences: SharedPreferences) : UserPreferences {

    override fun prefString(key: String, value: String?): String {
        with(sharedPreferences) {
            value?.let {
                edit().putString(key, value).apply()
            }
            return getString(key, DEFAULT_VALUE_STRING)!!
        }
    }

    override fun prefBoolean(key: String, value: Boolean?): Boolean {
        with(sharedPreferences) {
            value?.let {
                edit().putBoolean(key, value).apply()
            }
            return getBoolean(key, DEFAULT_VALUE_BOOLEAN)
        }
    }

    override fun prefInt(key: String, value: Int?): Int {
        with(sharedPreferences) {
            value?.let {
                edit().putInt(key, value).apply()
            }
            return getInt(key, DEFAULT_VALUE_INTEGER)
        }
    }

    override fun prefLong(key: String, value: Long?): Long {
        with(sharedPreferences) {
            value?.let {
                edit().putLong(key, value).apply()
            }
            return getLong(key, DEFAULT_VALUE_LONG)
        }
    }

    override fun prefFloat(key: String, value: Float?): Float {
        with(sharedPreferences) {
            value?.let {
                edit().putFloat(key, value).apply()
            }
            return getFloat(key, DEFAULT_VALUE_FLOAT)
        }
    }

}