/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.di

import android.app.UiModeManager
import android.content.Context
import com.bumptech.glide.Glide
import com.devsbitwise.android.common.data.data_source.local.BitmapToFileRepositoryImpl
import com.devsbitwise.android.common.data.data_source.local.UserPreferencesImpl
import com.devsbitwise.android.common.domain.repositories.BitmapToFileRepository
import com.devsbitwise.android.common.domain.repositories.UserPreferences
import com.devsbitwise.android.common.domain.use_cases.SaveBitmapUseCase
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

/**
 * This module provides instance of a library that is commonly used in an Android project.
 * */

val androidCommonModule = module {

    single {
        Glide.with(androidApplication())
    }

    single {
        androidApplication().getSharedPreferences("AppSharedPref", Context.MODE_PRIVATE)
    }

    single {
        androidApplication().getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
    }

    single {
        AppUpdateManagerFactory.create(androidApplication())
    }

    single<UserPreferences> {
        UserPreferencesImpl(get())
    }

    single<BitmapToFileRepository> {
        BitmapToFileRepositoryImpl(androidApplication())
    }

    singleOf(::SaveBitmapUseCase)

}