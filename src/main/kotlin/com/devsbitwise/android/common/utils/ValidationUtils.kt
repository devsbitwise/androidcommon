/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.utils

import android.util.Patterns
import com.devsbitwise.android.common.R
import com.devsbitwise.android.common.domain.models.ValidationResult

/**
 * Common validation use case
 * */
object ValidationUtils {

    /**
     * Typical email validation
     * */
    fun validateEmail(email: String?): ValidationResult {
        val emailInput: String? = email?.trim()
        return when {
            (emailInput.isNullOrBlank()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.field_required
                )
            }
            (Patterns.EMAIL_ADDRESS.matcher(emailInput).matches().not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.invalid_email
                )
            }
            else -> {
                ValidationResult(
                    isSuccessful = true
                )
            }
        }
    }

    /**
     * Typical password validation and requirements
     *
     * a digit must occur at least once
     * a lower case letter must occur at least once
     * an upper case letter must occur at least once
     * at least 8 characters
     * */
    fun validatePassword(password: String?): ValidationResult {
        val passwordInput: String? = password?.trim()
        return when {
            (passwordInput.isNullOrBlank()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.field_required
                )
            }
            (passwordInput.matches(".*\\d.*".toRegex()).not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.password_digit
                )
            }
            (passwordInput.matches(".*[a-z].*".toRegex()).not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.password_lowercase
                )
            }
            (passwordInput.matches(".*[A-Z].*".toRegex()).not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.password_uppercase
                )
            }
            (passwordInput.matches(".*[a-zA-Z].*".toRegex()).not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.password_letter
                )
            }
            (passwordInput.matches(".{8,}".toRegex()).not()) -> {
                ValidationResult(
                    isSuccessful = false,
                    errorMessageResource = R.string.password_length
                )
            }
            else -> {
                ValidationResult(
                    isSuccessful = true
                )
            }
        }
    }

}