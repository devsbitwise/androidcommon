/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat

const val PERMISSION_TAG = "PermissionExt"

/**
 * Extension for checking notification permission.
 * @return true if has permission else false
 * */
fun hasNotificationPermission(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        ContextCompat.checkSelfPermission(context, POST_NOTIFICATION) ==
                PackageManager.PERMISSION_GRANTED
    } else {
        true // Android 12 (S) and below has no runtime permission thus return true
    }
}

/**
 * Extension for checking image storage permission.
 * @return true if has permission else false
 * */
fun hasImageStoragePermission(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        ContextCompat.checkSelfPermission(context, READ_IMAGE_STORAGE) ==
                PackageManager.PERMISSION_GRANTED
    } else {
        hasStoragePermission(context)
    }
}

/**
 * Extension for checking video storage permission.
 * @return true if has permission else false
 * */
fun hasVideoStoragePermission(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        ContextCompat.checkSelfPermission(context, READ_VIDEO_STORAGE) ==
                PackageManager.PERMISSION_GRANTED
    } else {
        hasStoragePermission(context)
    }
}

/**
 * Extension for checking audio storage permission.
 * @return true if has permission else false
 * */
fun hasAudioStoragePermission(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        ContextCompat.checkSelfPermission(context, READ_AUDIO_STORAGE) ==
                PackageManager.PERMISSION_GRANTED
    } else {
        hasStoragePermission(context)
    }
}

/**
 * Extension for checking storage permission that supports Android 6 (M) below.
 * @return true if has permission else false
 * */
private fun hasStoragePermission(context: Context): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ContextCompat.checkSelfPermission(context, READ_EXT_STORAGE) ==
                PackageManager.PERMISSION_GRANTED
    } else {
        true // Android 5.1 (L) and below has no runtime permission thus return true
    }
}

/**
 * Extension for requesting notification permission on Android 13 (T) and above
 * */
fun ActivityResultLauncher<Array<String>>.requestNotificationPermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        launch(arrayOf(POST_NOTIFICATION))
    } else {
        Log.i(PERMISSION_TAG, "No need to request push notification permission.")
    }
}

/**
 * Extension for requesting storage permission on image.
 * */
fun ActivityResultLauncher<Array<String>>.requestImageStoragePermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
        launch(arrayOf(READ_IMAGE_STORAGE, READ_MEDIA_VISUAL_STORAGE_USER_SELECTED))
    }
    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        launch(arrayOf(READ_IMAGE_STORAGE))
    } else {
        requestStoragePermission()
    }
}

/**
 * Extension for requesting storage permission on video.
 * */
fun ActivityResultLauncher<Array<String>>.requestVideoStoragePermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
        launch(arrayOf(READ_VIDEO_STORAGE, READ_MEDIA_VISUAL_STORAGE_USER_SELECTED))
    }
    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        launch(arrayOf(READ_VIDEO_STORAGE))
    } else {
        requestStoragePermission()
    }
}

/**
 * Extension for requesting storage permission on audio.
 * */
fun ActivityResultLauncher<Array<String>>.requestAudioStoragePermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        launch(arrayOf(READ_AUDIO_STORAGE))
    } else {
        requestStoragePermission()
    }
}

/**
 * Extension for requesting storage permission on Android 12 (S) and below
 * */
private fun ActivityResultLauncher<Array<String>>.requestStoragePermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        launch(arrayOf(READ_EXT_STORAGE))
    } else {
        launch(arrayOf(READ_EXT_STORAGE, WRITE_EXT_STORAGE))
    }
}

const val READ_EXT_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE
const val WRITE_EXT_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE

@RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
const val READ_MEDIA_VISUAL_STORAGE_USER_SELECTED = Manifest.permission.READ_MEDIA_VISUAL_USER_SELECTED

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
const val READ_IMAGE_STORAGE = Manifest.permission.READ_MEDIA_IMAGES

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
const val READ_VIDEO_STORAGE = Manifest.permission.READ_MEDIA_VIDEO

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
const val READ_AUDIO_STORAGE = Manifest.permission.READ_MEDIA_AUDIO

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
const val POST_NOTIFICATION = Manifest.permission.POST_NOTIFICATIONS