/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView

/**
 * Extension for checking state of vertical scroll
 * @return true can scroll up
 * */
fun RecyclerView.canScrollUp(): Boolean {
    return this.canScrollVertically(-1)
}

/**
 * Extension for checking state of vertical scroll
 * @return true can scroll down
 * */
fun RecyclerView.canScrollDown(): Boolean {
    return this.canScrollVertically(1)
}

/**
 * Extension to smooth scroll in a given position that will also trigger behavior in views inside
 * `CoordinatorLayout` such as `BottomNavigationView`
 *
 * Note: There is a bug with `AppBarLayout` when using this solution,
 * it is recommended to trigger its behavior programmatically example using `setExpanded(true, false)`
 *
 * Reference: https://issuetracker.google.com/issues/313013679
 * */
fun RecyclerView.smoothScrollWithinCoordinator(position: Int) {
    this.startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL, ViewCompat.TYPE_NON_TOUCH)
    this.smoothScrollToPosition(position)
}