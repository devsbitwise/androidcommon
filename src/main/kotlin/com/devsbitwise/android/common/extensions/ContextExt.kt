/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat

/**
 * Extension for Toast
 * Can be use safely on any thread
 * */
fun Context.showToast(msg: String, length: Int = Toast.LENGTH_SHORT) {
    Handler(Looper.getMainLooper()).post { Toast.makeText(this, msg, length).show() }
}

/**
 * Extension for getting drawable resource that is compatible on almost all devices
 * regardless of configuration see https://stackoverflow.com/a/48237058/12204620
 * */
fun Context.resToDrawable(@DrawableRes resId: Int) =
    AppCompatResources.getDrawable(this, resId)

/**
 * Extension for getting color resource
 * */
fun Context.resToColor(@ColorRes resId: Int) =
    ContextCompat.getColor(this, resId)

/**
 * Extension for setting color state
 * */
fun Context.resToColorState(@ColorRes restId: Int): ColorStateList =
    AppCompatResources.getColorStateList(this, restId)

/**
 * Extension for Intent that depends on external apps or component.
 *
 * Note: Do not use context of window.decorView.rootView
 *
 * Related issues:
 *
 * https://stackoverflow.com/q/47730404/12204620
 *
 * https://stackoverflow.com/a/67355122/12204620
 *
 * @return null if no error and can handle the request.
 * */
fun Context.openExternalIntent(intent: Intent): Throwable? {
    return try {
        startActivity(intent)
        null
    } catch (e: Exception) {
        e
    }
}

/**
 * Extension for Intent that open the app settings screen.
 * @return null if no error and can handle the request.
 * */
fun Context.openSettingsIntent(): Throwable? {
    return try {
        with(Intent()) {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", packageName, null)
            startActivity(this)
        }
        null
    } catch (e: Exception) {
        e
    }
}

/**
 * Extension for Intent that shares image to external apps or component.
 * @return the Intent with payload containing the text and image.
 * */
fun Context.shareImageIntent(desc: String, uri: Uri): Intent {
    return Intent(Intent.ACTION_SEND).apply {
        type = "image/png"
        type = "text/plain"
        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        putExtra(Intent.EXTRA_STREAM, uri)
        putExtra(
            Intent.EXTRA_TEXT,
            "${desc}https://play.google.com/store/apps/details?id=${applicationContext.packageName}"
        )
    }
}
