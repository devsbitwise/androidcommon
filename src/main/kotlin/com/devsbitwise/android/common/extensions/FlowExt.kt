/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 * Flow is an asynchronous data stream that sequentially emits values and completes normally or with an exception.
 * The Flow interface does not carry information whether a flow is a cold stream that can be collected repeatedly and
 * triggers execution of the same code every time it is collected, or if it is a hot stream that emits different values
 * from the same running source on each collection. Usually flows represent cold streams, but there is a SharedFlow subtype
 * that represents hot streams. In addition to that, any flow can be turned into a hot one by the stateIn and shareIn operators,
 * or by converting the flow into a hot channel via the produceIn operator.
 * */

/**
 *
 * Difference between collect and collectLatest
 *
 * Flow Builder
 * fun intFlow() = flow {
 *   (1..5).forEach {
 *      delay(50)
 *        println("Flowing $it...")
 *        emit(it)
 *    }
 * }
 *
 * Collector that uses collect terminal operator
 * suspend fun main() {
 *    intFlow().collect {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  1 received
 *  Flowing 2...
 *  2 received
 *  Flowing 3...
 *  3 received
 *  Flowing 4...
 *  4 received
 *  Flowing 5...
 *  5 received
 *
 * Collector that uses collectLatest terminal operator
 * suspend fun main() {
 *    intFlow().collectLatest {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  Flowing 2...
 *  Flowing 3...
 *  Flowing 4...
 *  Flowing 5...
 *  5 received
 * */

/**
 * Extension collector for Flow using collect, ideal for UI state.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see Flow.collect
 * */
inline fun <T, L : Flow<T>> LifecycleOwner.collectFlow(
    flow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            withContext(Dispatchers.Main.immediate) {
                flow.collect { t -> function(t) }
            }
        }
    }
}

/**
 * Extension collector for Flow using collectLatest, ideal for data operation.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see Flow.collectLatest
 * */
inline fun <T, L : Flow<T>> LifecycleOwner.collectLatestFlow(
    flow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            withContext(Dispatchers.Main.immediate) {
                flow.collectLatest { t -> function(t) }
            }
        }
    }
}