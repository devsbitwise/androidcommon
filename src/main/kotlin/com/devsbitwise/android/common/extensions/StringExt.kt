/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.util.Patterns
import androidx.core.text.HtmlCompat
import java.util.Locale

/**
 * Extension for checking whether this String is a valid URL
 * */
fun String?.isValidUrl(): Boolean {
    return if (this.isNullOrBlank()) {
        false
    } else {
        Patterns.WEB_URL.matcher(this).matches()
    }
}

/**
 * Extension for checking whether this String is a valid email
 * */
fun String?.isValidEmail(): Boolean {
    return if (this.isNullOrBlank()) {
        false
    } else {
        Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }
}

/**
 * Extension for formatting words with title case
 * */
fun String.capitalizeWords(): String = split(" ").joinToString(" ") {
    it.replaceFirstChar { char ->
        char.titlecase(Locale.getDefault())
    }
}

/**
 * Extension for removing HTML script tag
 * */
fun String.strippedHtmlScriptTags() = this.replace(Regex("<script[^>]*>[\\s\\S]*?</script>"), "")

/**
 * Extension for removing HTML image tag to avoid square symbol https://stackoverflow.com/a/6385950
 * */
fun String.strippedHtmlImageTags() = this.replace(Regex("(<img[^>]*>)|(<(/)img>)"), "")

/**
 * Extension for removing any unsupported HTML tag on Android TextView
 * */
fun String.strippedHtmlUnsupportedTags() = HtmlCompat.fromHtml(this.strippedHtmlImageTags().strippedHtmlScriptTags(), HtmlCompat.FROM_HTML_MODE_LEGACY).toString()

/**
 * Extension for getting Spanned HTML
 * */
fun String.getHtmlSpanned() = HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)