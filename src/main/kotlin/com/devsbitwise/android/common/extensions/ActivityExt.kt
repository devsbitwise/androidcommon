/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Parcelable
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowInsets
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.IntentCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import java.io.Serializable

/**
 * Extension for dismissing Android's soft keyboard.
 * */
fun ComponentActivity.hideKeyboard() {
    WindowInsetsControllerCompat(window, window.decorView).hide(WindowInsetsCompat.Type.ime())
}

/**
 * Extension for startActivityForResult, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return ActivityResult and Context of the Activity.
 * */
inline fun ComponentActivity.activityForResult(crossinline function: (ActivityResult, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        function(result, this)
    }

/**
 * Extension for startActivityForResult, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return ActivityResult and Context of the Activity.
 * */
inline fun ComponentActivity.activityForResultIntentSender(crossinline function: (ActivityResult, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
        function(result, this)
    }

/**
 * Extension for multiple permission request, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return Map<String,Boolean> and Context of the Activity.
 * */
inline fun ComponentActivity.activityForResultMultiPermission(crossinline function: (Map<String, Boolean>, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
        Log.i("Permissions", permissions.toString())
        function(permissions, this)
    }

/**
 * Extension for getting device screen width in pixel.
 * */
fun Activity.getScreenWidth(outMetrics: DisplayMetrics): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        val windowMetrics = windowManager.currentWindowMetrics
        val insets = windowMetrics.windowInsets
            .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
        windowMetrics.bounds.width() - insets.left - insets.right
    } else {
        outMetrics.widthPixels
    }
}

/**
 * Extension for getting Parcelable Intent with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Activity.getParcelable(key: String): T? {
    return IntentCompat.getParcelableExtra(intent, key, T::class.java)
}

/**
 * Extension for getting Parcelable ArrayList Intent with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Activity.getParcelableArrayList(key: String): ArrayList<T>? {
    return IntentCompat.getParcelableArrayListExtra(intent, key, T::class.java)
}

/**
 * Extension for getting Parcelable Array Intent with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Activity.getParcelableArray(key: String): Array<T>? {
    return IntentCompat.getParcelableArrayExtra(intent, key, T::class.java)?.filterIsInstance<T>()?.toTypedArray()
}

/**
 * Extension for getting Serializable Intent with backward compatibility support.
 * */
inline fun <reified T : Serializable> Activity.getSerializableIntent(key: String): T? {
    return IntentCompat.getSerializableExtra(intent, key, T::class.java)
}