/*
 * Copyright (c) 2023 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.extensions

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Locale
import kotlin.math.abs

/**
 * Check if the given number is negative
 * */
fun Number?.isNegative(): Boolean {

    if (this == null) {
        return false
    }

    return when (this.toDouble().compareTo(0.00)) {
        -1 -> true
        else -> false
    }

}

/**
 * Formatter that supports value with fractional lower than hundredths.
 * Ideal use for handling currency value
 * */
fun Number?.formatFractional(): String {

    var isNegative = false

    if (this == null) {
        return "0.00"
    }

    var nonNullNumber = this.toDouble()

    if (nonNullNumber < 0) {
        isNegative = true
        nonNullNumber = abs(this.toDouble())
    }

    var temp = (nonNullNumber - nonNullNumber.toInt()).times(100)

    var nrDec = 2

    val format = NumberFormat.getInstance(Locale.US)

    if (temp >= 1.0) {
        format.maximumFractionDigits = nrDec
        return if (isNegative) "-${format.format(nonNullNumber)}" else format.format(nonNullNumber)
    }

    // Supports up to 15 accuracy
    while (temp < 1.0 && nrDec < 15) {
        temp *= 10
        nrDec++
    }

    if ((temp * 10 % 10) != 0.0) {
        nrDec++
    }

    format.maximumFractionDigits = nrDec

    return if (isNegative) "-${format.format(nonNullNumber)}" else format.format(nonNullNumber)

}

/**
 * Formatter that return value in two decimal places
 * */
fun Number?.formatTwoDecimal(hasComma: Boolean): String {

    if (this == null) {
        return "0.00"
    }

    if (hasComma) {
        return DecimalFormat("#,##0.00").format(this)
    }

    return DecimalFormat("0.00").format(this)

}

/**
 * Formatter with units : K, M, B, T, P, E
 * */
fun Number?.formatWithUnit(): String {

    if (this == null) {
        return "0.00"
    }

    var nonNullNumber = this.toDouble()
    val arr = arrayOf("", "K", "M", "B", "T", "P", "E")
    var index = 0

    while (nonNullNumber / 1_000 >= 1) {
        nonNullNumber /= 1_000
        index++
    }

    val decimalFormat = DecimalFormat("#.##")

    return String.format("%s%s", decimalFormat.format(nonNullNumber), arr[index])

}