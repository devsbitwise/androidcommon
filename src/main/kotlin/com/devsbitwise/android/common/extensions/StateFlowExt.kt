/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * StateFlow is a hot flow that needs an initial value and emits it as soon as a collector starts collecting.
 * It only emits the last value, and it does not emit consecutive repeated values.
 * In other words, it only emits values that are distinct from the previous item.
 * It is a special-purpose, high-performance, and efficient implementation of SharedFlow for the narrow,
 * but widely used case of sharing a state thus it has more limited/restricted configuration options.
 * It works similarly to LiveData with `repeatOnLifecycle`.
 * */

/**
 *
 * Difference between collect and collectLatest
 *
 * Flow Builder
 * fun intFlow() = flow {
 *   (1..5).forEach {
 *      delay(50)
 *        println("Flowing $it...")
 *        emit(it)
 *    }
 * }
 *
 * Collector that uses collect terminal operator
 * suspend fun main() {
 *    intFlow().collect {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  1 received
 *  Flowing 2...
 *  2 received
 *  Flowing 3...
 *  3 received
 *  Flowing 4...
 *  4 received
 *  Flowing 5...
 *  5 received
 *
 * Collector that uses collectLatest terminal operator
 * suspend fun main() {
 *    intFlow().collectLatest {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  Flowing 2...
 *  Flowing 3...
 *  Flowing 4...
 *  Flowing 5...
 *  5 received
 * */

/**
 * Extension collector for StateFlow using collect, ideal for UI state.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see StateFlow.collect
 * */
inline fun <T, L : StateFlow<T>> LifecycleOwner.collectState(
    stateFlow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            stateFlow.collect { t -> function(t) }
        }
    }
}

/**
 * Extension collector for StateFlow using collectLatest, ideal for data operation.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see StateFlow.collectLatest
 * */
inline fun <T, L : StateFlow<T>> LifecycleOwner.collectLatestState(
    stateFlow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            stateFlow.collectLatest { t -> function(t) }
        }
    }
}