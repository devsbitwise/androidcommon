/*
 * Copyright (c) 2024 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


/**
 * SharedFlow is a hot flow that does not needs an initial value.
 * It emits all the values and does not care if the value is distinct from the previous item.
 * SharedFlow is a Flow that allows for sharing itself between multiple collectors,
 * so that only one flow is effectively run (materialized) for all of the simultaneous collectors.
 * If you define a SharedFlow that accesses databases and it is collected by multiple collectors,
 * the database access can only run once, and the resulting data will be shared to all collectors.
 * It can work similarly to StateFlow if replay parameter is set to 1.
 * */

/**
 *
 * Difference between collect and collectLatest
 *
 * Flow Builder
 * fun intFlow() = flow {
 *   (1..5).forEach {
 *      delay(50)
 *        println("Flowing $it...")
 *        emit(it)
 *    }
 * }
 *
 * Collector that uses collect terminal operator
 * suspend fun main() {
 *    intFlow().collect {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  1 received
 *  Flowing 2...
 *  2 received
 *  Flowing 3...
 *  3 received
 *  Flowing 4...
 *  4 received
 *  Flowing 5...
 *  5 received
 *
 * Collector that uses collectLatest terminal operator
 * suspend fun main() {
 *    intFlow().collectLatest {
 *      delay(100)
 *      println("\t\t\t\t$it received")
 *    }
 * }
 *
 * Result:
 *  Flowing 1...
 *  Flowing 2...
 *  Flowing 3...
 *  Flowing 4...
 *  Flowing 5...
 *  5 received
 * */

/**
 * Extension collector for SharedFlow using collect, ideal for UI state.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see SharedFlow.collect
 * */
inline fun <T, L : SharedFlow<T>> LifecycleOwner.collectShared(
    sharedFlow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            sharedFlow.collect { t -> function(t) }
        }
    }
}

/**
 * Extension collector for SharedFlow using collectLatest, ideal for data operation.
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 *
 * @param lifecycleState Lifecycle state where you want to start collecting flow, default is STARTED
 * @see SharedFlow.collectLatest
 * */
inline fun <T, L : SharedFlow<T>> LifecycleOwner.collectLatestShared(
    sharedFlow: L,
    crossinline function: (T) -> Unit,
    lifecycleState: Lifecycle.State = Lifecycle.State.STARTED
) {
    lifecycleScope.launch {
        repeatOnLifecycle(lifecycleState) {
            sharedFlow.collectLatest { t -> function(t) }
        }
    }
}

/**
 * Extension for MutableSharedFlow that has similar behavior with MutableLiveData
 * */
@Suppress("FunctionName")
fun <T> MutableSharedFlowLikeLiveData() =
    MutableSharedFlow<T>(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)