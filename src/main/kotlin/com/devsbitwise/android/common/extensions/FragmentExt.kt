/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.content.Context
import android.os.Parcelable
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.BundleCompat
import androidx.fragment.app.Fragment
import java.io.Serializable

/**
 * Extension for startActivityForResult, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return ActivityResult and Context of the Fragment.
 * */
inline fun Fragment.activityForResult(crossinline function: (ActivityResult, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        function(result, requireContext())
    }

/**
 * Extension for startActivityForResult, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return ActivityResult and Context of the Activity.
 * */
inline fun Fragment.activityForResultIntentSender(crossinline function: (ActivityResult, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
        function(result, requireContext())
    }

/**
 * Extension for multiple permission request, replacing the deprecated onActivityResult.
 *
 * Note:
 *
 * Need to register ahead of time and not immediately before you call [ActivityResultLauncher.launch].
 * When Activity was destroyed and recreated due to configuration change, calling `launch` will cause IllegalStateException.
 * To avoid such issue, it is recommended to initialize it inside onCreate or onStart to ensure that you are using
 * a fresh and new instance of ActivityResultLauncher
 *
 * @return Map<String,Boolean> and Context of the Fragment.
 * */
inline fun Fragment.activityForResultMultiPermission(crossinline function: (Map<String, Boolean>, Context) -> Unit) =
    registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
        Log.i("Permissions", permissions.toString())
        function(permissions, requireContext())
    }

/**
 * Extension for getting Parcelable Bundle with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Fragment.getParcelable(key: String): T? {
    return arguments?.let { BundleCompat.getParcelable(it, key, T::class.java) }
}

/**
 * Extension for getting Parcelable ArrayList Bundle with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Fragment.getParcelableArrayList(key: String): ArrayList<T>? {
    return arguments?.let { BundleCompat.getParcelableArrayList(it, key, T::class.java) }
}

/**
 * Extension for getting Parcelable Array Bundle with backward compatibility support.
 * */
inline fun <reified T : Parcelable> Fragment.getParcelableArray(key: String): Array<T>? {
    return arguments?.let { BundleCompat.getParcelableArray(it, key, T::class.java) }?.filterIsInstance<T>()?.toTypedArray()
}

/**
 * Extension for getting Serializable Bundle with backward compatibility support.
 * */
inline fun <reified T : Serializable> Fragment.getSerializableIntent(key: String): T? {
    return arguments?.let { BundleCompat.getSerializable(it, key, T::class.java) }
}