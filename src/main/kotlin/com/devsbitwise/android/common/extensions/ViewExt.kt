/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.View
import androidx.annotation.ColorRes


/**
 * Extension for generating Bitmap of View
 * @param backgroundColor - background color of the image
 * @return Bitmap equivalent of the view
 * */
fun View.createBitmapFromView(@ColorRes backgroundColor: Int): Bitmap {

    // Define a bitmap with the same size as the view
    return Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888).apply {

        // Bind a canvas to it
        val canvas = Canvas(this)
        canvas.drawColor(context.resToColor(backgroundColor))
        // Draw the view on the canvas
        draw(canvas)

    }

}

/**
 * Extension for toggling visibility of the view
 * @return current visibility of this View after calling this method, one of [View.VISIBLE], [View.INVISIBLE], or [View.GONE]
 * */
fun View.showOrHide(hideBehavior: Int = View.GONE): Int {
    visibility = if (visibility == View.VISIBLE) hideBehavior else View.VISIBLE
    return visibility
}
