/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * Extension observer for Nullable LiveData
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 * */
inline fun <T, L : LiveData<T?>> LifecycleOwner.observe(
    liveData: L,
    crossinline function: (T?) -> Unit
) {
    liveData.observe(this) { t -> function(t) }
}

/**
 * Extension observer for Non-null LiveData
 *
 * Make sure it's launched with viewLifecycleOwner for Fragments instead of Fragment.lifecycle (this)
 * https://medium.com/androiddevelopers/repeatonlifecycle-api-design-story-8670d1a7d333
 * */
inline fun <T, L : LiveData<T>> LifecycleOwner.observeNonNull(
    liveData: L,
    crossinline function: (T) -> Unit
) {
    liveData.observe(this) { t -> function(t) }
}