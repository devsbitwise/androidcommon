/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.extensions

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

const val NOTIFICATION_TAG = "NotificationExt"

/**
 * Extension function to create Notification Channel
 * @param channelName is the channel id and channel name of notification
 * @param ledColor is the LED color when this notification is visible
 * @param vibratePattern is the vibration pattern when this notification was triggered
 * @param soundUri is the URI to the audio file used for this notification channel
 * @param audioAttr define how the audio system handles routing, volume, and focus decisions for the specified source
 * */
fun NotificationManagerCompat.createChannel(
    channelName: String,
    ledColor: Int = Color.BLUE,
    vibratePattern: LongArray = longArrayOf(1_000L, 1_000L, 1_000L, 1_000L, 1_000L),
    soundUri: Uri = Settings.System.DEFAULT_NOTIFICATION_URI,
    audioAttr: AudioAttributes = Notification.AUDIO_ATTRIBUTES_DEFAULT
) {

    // In Android 8 (O) and above notification channel is needed.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

        val channel = NotificationChannel(
            channelName,
            channelName,
            NotificationManager.IMPORTANCE_HIGH
        ).apply {

            setShowBadge(true)
            enableLights(true)
            enableVibration(true)

            setSound(soundUri, audioAttr)
            lightColor = ledColor
            vibrationPattern = vibratePattern
            lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC

        }

        createNotificationChannel(channel)

    }
    else {
        Log.i(NOTIFICATION_TAG, "No need to request notification channel.")
    }

}

/**
 * Extension function to check the availability of Notification Channel
 * @param context the Context that will provide access to resources
 * @param channelId channel id of notification to check
 * */
fun NotificationManagerCompat.isNotificationChannelEnabled(
    context: Context,
    channelId: String
): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        if (TextUtils.isEmpty(channelId).not()) {
            val channel = getNotificationChannel(channelId)
            if (channel != null) {
                return channel.importance != NotificationManager.IMPORTANCE_NONE &&
                        NotificationManagerCompat.from(context).areNotificationsEnabled() // A channel could be enabled while the overall notifications are disabled
            }
        }
        false
    } else {
        NotificationManagerCompat.from(context).areNotificationsEnabled()
    }
}