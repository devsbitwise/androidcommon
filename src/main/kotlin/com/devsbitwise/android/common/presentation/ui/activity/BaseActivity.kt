/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.presentation.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.ComponentActivity
import androidx.annotation.EmptySuper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.devsbitwise.android.common.extensions.activityForResultMultiPermission

/**
 * Activity base class
 * */
abstract class BaseActivity<VB : ViewBinding, VM : ViewModel> : AppCompatActivity() {

    private var _binding: VB? = null

    // This property is only valid between onCreate and onDestroy.
    private val binding get() = _binding!!

    /**
     * Abstract member variable to set the nullable ViewModel.
     * */
    protected abstract val viewModel: VM?

    /**
     * Abstract method to set the ViewBinding.
     * */
    protected abstract fun bindingInflater(): (LayoutInflater) -> VB

    /**
     * Abstract method where views can be initialize.
     * */
    protected abstract fun initViews(binding: VB)

    /**
     * Abstract method where observables like LiveData or Flow can be set.
     * */
    protected abstract fun subscribeUI(savedInstanceState: Bundle?)

    /**
     * Callback where rebinding can be made when [ComponentActivity.onNewIntent] is called.
     * Override this when you want to bind views with the Activity's new intent data.
     * */
    @EmptySuper
    open fun bindNewIntent(binding: VB, intent: Intent) {
        /* no-op */
    }

    /**
     * Callback where Splash API can be initialize.
     * Override this when you want to install the Splash Screen API.
     * */
    @EmptySuper
    open fun installSplash() {
        /* no-op */
    }

    /**
     * Results from multiple runtime permission requests.
     * Override this when you want to receive the requested permission result.
     * */
    @EmptySuper
    open fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {
        /* no-op */
    }

    /**
     * Method for requesting multiple runtime permission.
     * Need to register ahead of time and not immediately before you launch.
     * @return ActivityResultLauncher<Array<String>>
     * */
    protected fun getPermission() = activityForResultMultiPermission { permissions, context ->
        // Safe cast as it is guaranteed that this context comes from Activity
        onPermissionResult(permissions, context as ComponentActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplash()
        super.onCreate(savedInstanceState)
        _binding = bindingInflater().invoke(layoutInflater)
        setContentView(binding.root)
        initViews(binding)
        subscribeUI(savedInstanceState)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        bindNewIntent(binding, intent)
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

}