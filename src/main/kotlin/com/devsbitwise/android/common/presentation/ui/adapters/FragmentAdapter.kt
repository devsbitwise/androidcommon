/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

@file:Suppress("unused", "RedundantSuppression")

package com.devsbitwise.android.common.presentation.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * FragmentAdapter for ViewPager2
 * */
class FragmentAdapter : FragmentStateAdapter {

    private var fragmentList: MutableList<Pair<String, Fragment>>

    constructor(
        fragmentList: MutableList<Pair<String, Fragment>>,
        fragmentManager: FragmentManager,
        lifecycle: Lifecycle
    ) : super(fragmentManager, lifecycle) {
        this.fragmentList = fragmentList
    }

    constructor(
        fragmentList: MutableList<Pair<String, Fragment>>,
        activity: FragmentActivity
    ) : super(activity) {
        this.fragmentList = fragmentList
    }

    override fun getItemCount(): Int = fragmentList.size

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position].second
    }

    fun getFragmentTabName(position: Int) = fragmentList[position].first

    fun addFragment(fragment: Pair<String, Fragment>) {
        fragmentList.add(fragment)
        notifyItemInserted(fragmentList.size.minus(1))
        notifyItemRangeChanged(fragmentList.size.minus(1), fragmentList.size)
    }

    fun removeFragment(position: Int) {
        fragmentList.removeAt(position)
        notifyItemRemoved(fragmentList.size.plus(1))
        notifyItemRangeChanged(position.minus(1), fragmentList.size)
    }

}
