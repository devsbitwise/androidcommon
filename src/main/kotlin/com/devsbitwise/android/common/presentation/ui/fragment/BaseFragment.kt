/*
 * Copyright (c) 2022 Devs Bitwise
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.devsbitwise.android.common.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.annotation.EmptySuper
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.devsbitwise.android.common.extensions.activityForResultMultiPermission

/**
 * Fragment base class
 * */
abstract class BaseFragment<VB : ViewBinding, VM : ViewModel> : Fragment() {

    private var _binding: VB? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var onBackPressedCallback: OnBackPressedCallback

    /**
     *
     * Using OnBackPressedCallback
     *
     * True - This Fragment will handle the back press event. Override [onBackPressed] in this Fragment to modify its behavior.
     *
     * False - The host Activity or parent Fragment (if there is any) will handle the back press event.
     *
     * */
    protected abstract val interceptBackPress: Boolean

    /**
     * Abstract member variable to set the nullable ViewModel.
     * */
    protected abstract val viewModel: VM?

    /**
     * Abstract method to set the ViewBinding.
     * */
    protected abstract fun bindingInflater(): (LayoutInflater, ViewGroup?, Boolean) -> VB

    /**
     * Abstract method where views can be initialize.
     * */
    protected abstract fun initViews(binding: VB)

    /**
     * Abstract method where observables like LiveData or Flow can be set.
     * */
    protected abstract fun subscribeUI(savedInstanceState: Bundle?)

    /**
     * Custom onBackPressed for Fragment similar from that of Activity.
     * */
    open fun onBackPressed() {
        onBackPressedCallback.isEnabled = false // Disable this Fragment's back press
        requireActivity().onBackPressedDispatcher.onBackPressed() // Trigger the next onBackPressedCallback
    }

    /**
     * Results from multiple runtime permission requests.
     * Override this when you want to receive the requested permission result.
     * */
    @EmptySuper
    open fun onPermissionResult(permissions: Map<String, Boolean>, activity: ComponentActivity) {
        /* no-op */
    }

    /**
     * Method for requesting multiple runtime permission.
     * Need to register in onCreate before calling launch.
     * @return ActivityResultLauncher<Array<String>>
     * */
    protected fun getPermission() = activityForResultMultiPermission { permissions, context ->
        // Safe cast as it is guaranteed that this context comes from Activity
        onPermissionResult(permissions, context as ComponentActivity)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater().invoke(inflater, container, false)
        initViews(binding)

        // Set `enabled` to false so callbacks will work only when this Fragment is visible to user
        // which is handled in `onResume`
        onBackPressedCallback = object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner, // Lifecycle owner, so no need to manually call OnBackPressedCallback.remove()
            onBackPressedCallback
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeUI(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        // Re-enable intercepting back press when the Fragment is visible to user
        onBackPressedCallback.isEnabled = interceptBackPress
    }

    override fun onPause() {
        super.onPause()
        // Disable intercepting back press when the Fragment is not visible to user
        onBackPressedCallback.isEnabled = false
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}