package com.devsbitwise.android.common.domain.models

import androidx.annotation.StringRes

data class ValidationResult(
    val isSuccessful: Boolean,
    @StringRes val errorMessageResource: Int? = null,
    val errorMessage: String? = null
)