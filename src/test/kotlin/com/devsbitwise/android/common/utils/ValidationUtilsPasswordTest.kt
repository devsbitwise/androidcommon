package com.devsbitwise.android.common.utils

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class ValidationUtilsPasswordTest {

    @Test
    fun `Empty password returns false`() {
        val result = ValidationUtils.validatePassword("")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Missing at least 1 digit in password returns false`() {
        val result = ValidationUtils.validatePassword("qwertyUi")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Missing lowercase letter in password returns false`() {
        val result = ValidationUtils.validatePassword("QWERTY12")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Missing uppercase letter in password returns false`() {
        val result = ValidationUtils.validatePassword("qwerty12")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Missing letter in password returns false`() {
        val result = ValidationUtils.validatePassword("123456")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Less than 8 characters in password returns false`() {
        val result = ValidationUtils.validatePassword("qwertY1")
        assertThat(result.isSuccessful).isFalse()
    }

    @Test
    fun `Correct format of password returns true`() {
        val result = ValidationUtils.validatePassword("qwertyU12")
        assertThat(result.isSuccessful).isTrue()
    }

}